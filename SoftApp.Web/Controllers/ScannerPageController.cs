﻿using SoftApp.Common.Models;
using SoftApp.Web.App_Code.ScannerPage;
using System.Net;
using System.Web.Http;

namespace SoftApp.Web.Controllers
{
    [RoutePrefix("soft")]
    public class ScannerPageController : ApiController
    {
        [HttpPost]
        [Route("ScannerPage")]
        public IHttpActionResult ScannerPage(ScannerPageRequest scannerPageRequest)
        {
            if (scannerPageRequest == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            //Variable de Respuesta
            ScannerPageResponse scannerPageResponse = new ScannerPageResponse();

            //Obtengo la respuesta
            scannerPageResponse = clsScannerPage.ScannerPage(scannerPageRequest);

            return Ok(scannerPageResponse);


        }
    }
}