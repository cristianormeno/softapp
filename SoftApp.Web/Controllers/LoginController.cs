﻿namespace SoftApp.Web.Controllers
{
    using SoftApp.Common.Models;
    using SoftApp.Web.App_Code.Login;
    using System.Net;
    using System.Web.Http;

    [RoutePrefix("soft")]

    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("Login")]
        public IHttpActionResult LogIn(LoginRequest loginRequest)
        {
            if (loginRequest == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }

            //Variable de Respuesta
            LoginResponse Respuesta = new LoginResponse();

            //Obtengo la respuesta
            Respuesta = clsLogin.Login(loginRequest);

            return Ok(Respuesta);


        }
    }
}