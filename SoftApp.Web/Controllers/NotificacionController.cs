﻿namespace SoftApp.Web.Controllers
{
    using SoftApp.Web.App_Code.Notification;
    using SoftApp.Web.Models;
    using System;
    using System.Web.Http;


    [RoutePrefix("soft")]

    public class NotificacionController : ApiController
    {
        [HttpPost]
        [Route("EnviarNotificacion")]
        public IHttpActionResult EnviarNotificacion(NotificacionRequest Datos)
        {
            //Variable de Respuesta
            NotificacionResponse Respuesta = new NotificacionResponse();

            try
            {

                // obtengo la respuesta
                Respuesta = clsNotification.EnviarNotificacion(Datos);
                return Ok(Respuesta);
            }
            catch (Exception ex)
            {
                Respuesta.Respuesta.Resultado = "ERROR";
                Respuesta.Respuesta.Mensaje = ex.Message;
                return Ok(Respuesta);
            }


        }

    }
}