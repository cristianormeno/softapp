﻿namespace SoftApp.Web.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;



    public class NotificacionRequest
    {
        public NotificacionRequestMensaje Mensaje { get; set; }
        public NotificacionRequestDispositivo Dispositivo { get; set; }

    }

    public partial class NotificacionRequestMensaje
    {
        [Required]
        [Display(Description = "Titulo")]
        public string Titulo { get; set; } = string.Empty;

        [Required]
        [Display(Description = "Mensaje")]
        public string Mensaje { get; set; } = string.Empty;

        [Required]
        [Display(Description = "MensajeHtml")]
        public string MensajeHtml { get; set; } = string.Empty;

        [Required]
        [Display(Description = "Semaforo")]
        public string Semaforo { get; set; } = string.Empty;
    }

    public partial class NotificacionRequestDispositivo
    {
        [Required]
        [Display(Description = "Token")]
        public string Token { get; set; } = string.Empty;

        [Required]
        [Display(Description = "Sistema")]
        public int Sistema { get; set; } = 0;
    }

    public class NotificacionResponse
    {
        public NotificacionResponseResultado Respuesta { get; set; }

    }

    public partial class NotificacionResponseResultado
    {
        [JsonProperty("Resultado")]
        public string Resultado { get; set; } = string.Empty;

        [JsonProperty("Mensaje")]
        public string Mensaje { get; set; } = string.Empty;
    }

    public class NotificacionSendRequest
    {
        public string Titulo { get; set; }
        public string Mensaje { get; set; }
        public string MensajeHtml { get; set; }
        public string Token { get; set; }
        public int Sistema { get; set; }
        public string NotificacionId { get; set; }
        public string Semaforo { get; set; }
    }
}