﻿namespace SoftApp.Web.Models
{
    public class SqlParametro
    {
        public string Name { get; set; } = string.Empty;
        public System.Data.SqlDbType Type { get; set; }
        public int Size { get; set; } = 0;
        public System.Data.ParameterDirection Direction { get; set; }
        public object Value { get; set; }



    }
}
