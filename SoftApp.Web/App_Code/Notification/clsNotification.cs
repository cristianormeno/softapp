﻿namespace SoftApp.Web.App_Code.Notification
{
    using SoftApp.Web.Models;
    using System;
    public class clsNotification
    {
        public static NotificacionResponse EnviarNotificacion(NotificacionRequest Datos)
        {
            //Variable de Respuesta
            NotificacionResponse Respuesta = new NotificacionResponse()
            {
                Respuesta = new NotificacionResponseResultado()
            };

            if (!string.IsNullOrEmpty(Datos.Mensaje.MensajeHtml))
            {
                // Cuando viene mensaje html, al mensaje lo relleno con el título
                Datos.Mensaje.Mensaje = Datos.Mensaje.Titulo;
            }

            try
            {
                if (string.IsNullOrEmpty(Datos.Mensaje.Titulo))
                {
                    Respuesta.Respuesta.Resultado = "ERROR";
                    Respuesta.Respuesta.Mensaje = "Título vacío";
                    return Respuesta;
                }
                else
                {
                    if (string.IsNullOrEmpty(Datos.Mensaje.Mensaje))
                    {
                        Respuesta.Respuesta.Resultado = "ERROR";
                        Respuesta.Respuesta.Mensaje = "Mensaje vacío";
                        return Respuesta;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(Datos.Dispositivo.Token))
                        {
                            Respuesta.Respuesta.Resultado = "ERROR";
                            Respuesta.Respuesta.Mensaje = "Token vacío";
                            return Respuesta;
                        }
                        else
                        {
                            if (Datos.Dispositivo.Sistema != 1 && Datos.Dispositivo.Sistema != 2)
                            {
                                Respuesta.Respuesta.Resultado = "ERROR";
                                Respuesta.Respuesta.Mensaje = "Sistema erróneo";
                                return Respuesta;
                            }
                        }
                    }
                }

                //Preparo la notificación para el envío
                NotificacionSendRequest notificacion = new NotificacionSendRequest()
                {
                    Titulo = Datos.Mensaje.Titulo.Trim(),
                    Mensaje = Datos.Mensaje.Mensaje.Trim(),
                    MensajeHtml = Datos.Mensaje.MensajeHtml.Trim(),
                    Semaforo = Datos.Mensaje.Semaforo.Trim(),
                    Token = Datos.Dispositivo.Token.Trim(),
                    Sistema = Datos.Dispositivo.Sistema,
                    NotificacionId = Guid.NewGuid().ToString()
                };

                string ErrorID = ""; string ErrorMensaje = "";
                FirebaseSend fb = new FirebaseSend();
                fb.SendNotificacion(notificacion, ref ErrorID, ref ErrorMensaje);

                if (string.IsNullOrEmpty(ErrorMensaje))
                {
                    Respuesta.Respuesta.Resultado = "OK";
                    Respuesta.Respuesta.Mensaje = "Envío correcto";
                    return Respuesta;
                }
                else
                {
                    Respuesta.Respuesta.Resultado = "ERROR";
                    Respuesta.Respuesta.Mensaje = ErrorMensaje.Trim();
                    return Respuesta;
                }
            }
            catch (Exception ex)
            {
                Respuesta.Respuesta.Resultado = "ERROR";
                Respuesta.Respuesta.Mensaje = ex.Message.Trim();
                return Respuesta;
            }

        }
    }
}