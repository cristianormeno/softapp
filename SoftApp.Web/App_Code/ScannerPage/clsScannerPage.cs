﻿using SoftApp.Common.Models;
using System;

namespace SoftApp.Web.App_Code.ScannerPage
{
    public class clsScannerPage
    {
        public static ScannerPageResponse ScannerPage(ScannerPageRequest scannerPageRequest)
        {
            ScannerPageResponse respuesta = new ScannerPageResponse();
            switch (scannerPageRequest.Sistema.ToLower())
            {
                case "magento":
                    switch (scannerPageRequest.Destino.ToLower()) 
                    {
                        case "productomagento":
                            respuesta.URL = "http://tiendas.solucioneserp.net/lamaral/poliflex-membranas.html";
                            break;
                    }
                    
                    break;
                case "tickets":
                    switch (scannerPageRequest.Destino.ToLower())
                    {
                        case "pruebacodigo":
                            respuesta.URL = "http://tickets.solucioneserp.net/pruebacodigo.html?id=xxxcodxxx".Replace("xxxcodxxx", scannerPageRequest.Valor.Trim());
                            break;
                    }
                    break;
            }


            return respuesta;
        }
    }
}