﻿namespace SoftApp.Web.App_Code.Login
{
    using SoftApp.Common.Models;
    using SoftApp.Web.Helpers;
    using SoftApp.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;

    public static class dalLogin
    {
        public static DataSet Login(string conexion, LoginRequest loginRequest)
        {
            DataSet dataSet = new DataSet();
            try
            {
                //Preparo el parámetro json
                string parametros = Newtonsoft.Json.JsonConvert.SerializeObject(loginRequest);

                List<SqlParametro> Parametros = new List<SqlParametro>();

                SqlParametro sqlParametro = new SqlParametro()
                {
                    Name = "@Parametros",
                    Type = SqlDbType.VarChar,
                    Direction = ParameterDirection.Input,
                    Size = -1,
                    Value = parametros
                };

                Parametros.Add(sqlParametro);
                dataSet = Sql.sqlQueryProcedure(conexion, "spLogin", Parametros);
            }
            catch (Exception ex)
            { }



            return dataSet;






        }
    }


}