﻿namespace SoftApp.Web.Helpers
{
    using SoftApp.Web.Models;
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    public static class  Sql
    {
        public static DataSet sqlQueryProcedure(string conexion, string procedimiento, List<SqlParametro> parametros, int timeOut = 360)
        {

            // Seteo la conexión
            SqlConnection sqlConnection = new SqlConnection(conexion);

            //Seteo el comando
            SqlCommand sqlCommand = new SqlCommand()
            {
                CommandType = CommandType.StoredProcedure,
                CommandText = procedimiento,
                Connection = sqlConnection,
                CommandTimeout = timeOut                
            };

            // Agrego parámetros
            foreach (SqlParametro item in parametros)
            {
                try
                {
                    SqlParameter sqlParameter = new SqlParameter()
                    {
                        ParameterName = item.Name,
                        SqlDbType = item.Type,
                        Size = item.Size,
                        Direction = item.Direction,
                        Value=item.Value,
                    };
                    sqlCommand.Parameters.Add(sqlParameter);
                }
                catch (Exception ex) { }
            }

            //abrir la conexión
            sqlConnection.Open();

            // Obtengo los datos y los coloco en un dataset
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            DataSet dataSet = new DataSet();
            sqlDataAdapter.SelectCommand = sqlCommand;
            sqlDataAdapter.Fill(dataSet);

            //Cierro la conexión
            sqlConnection.Close();
            sqlCommand.Connection.Dispose();

            return dataSet;
        }
    }
}
