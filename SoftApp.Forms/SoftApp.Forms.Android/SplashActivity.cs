﻿namespace SoftApp.Forms.Droid
{

    using Android.App;
    using Android.Content;
    using Android.OS;
    using Plugin.FirebasePushNotification;

    [Activity(Theme = "@style/Theme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);


            System.Threading.Thread.Sleep(2000);

            this.StartActivity(typeof(MainActivity));

            FirebasePushNotificationManager.ProcessIntent(this, Intent);
        }

        protected override void OnNewIntent(Intent intent)
        {
            FirebasePushNotificationManager.ProcessIntent(this, intent);
            base.OnNewIntent(intent);
        }
    }
}