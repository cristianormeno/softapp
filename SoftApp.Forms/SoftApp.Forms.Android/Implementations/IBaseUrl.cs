﻿using SoftApp.Forms.Droid.Implementations;
using SoftApp.Forms.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(IBaseUrlAndroid))]

namespace SoftApp.Forms.Droid.Implementations
{
    public class IBaseUrlAndroid : IBaseUrl
    {
        public string Get()
        {
            return "file:///android_asset/";
        }
    }
}