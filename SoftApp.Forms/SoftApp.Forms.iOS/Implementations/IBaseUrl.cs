﻿using Foundation;
using SoftApp.Forms.Interfaces;
using SoftApp.Forms.iOS.Implementations;
using Xamarin.Forms;

[assembly: Dependency(typeof(IBaseUrl_iOS))]
namespace SoftApp.Forms.iOS.Implementations
{
    public class IBaseUrl_iOS : IBaseUrl
    {
        public string Get()
        {
            return NSBundle.MainBundle.BundlePath;
        }
    }
}