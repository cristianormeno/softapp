﻿namespace SoftApp.Forms.iOS
{
    using Firebase.CloudMessaging;
    using Foundation;
    using System;
    using UIKit;
    using UserNotifications;

    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate, IUNUserNotificationCenterDelegate, IMessagingDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            //Inicializar ZXing.Net.Mobile para QR Code
            ZXing.Net.Mobile.Forms.iOS.Platform.Init();

            LoadApplication(new App());

            Firebase.Core.App.Configure();

            // Register your app for remote notifications.
            RegisterForRemoteNotifications();

            UIApplication.SharedApplication.RegisterForRemoteNotifications();

            return base.FinishedLaunching(app, options);
        }

        private void RegisterForRemoteNotifications()
        {
            // Create action z
            var actionID = "check";
            var title = "Check";
            var action = UNNotificationAction.FromIdentifier(actionID, title, UNNotificationActionOptions.Foreground);

            // Create category
            var categoryID = "notification";
            var actions = new UNNotificationAction[] { action };
            var intentIDs = new string[] { };
            //var categoryOptions = new UNNotificationCategoryOptions[] { };
            var category = UNNotificationCategory.FromIdentifier(categoryID, actions, intentIDs, UNNotificationCategoryOptions.None);

            // Register category
            var categories = new UNNotificationCategory[] { category };
            UNUserNotificationCenter.Current.SetNotificationCategories(new NSSet<UNNotificationCategory>(categories));

            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert
                                                                  | UNAuthorizationOptions.Badge
                                                                  | UNAuthorizationOptions.Sound,
                                                                  (a, err) => {
                                                                      //TODO handle error
                                                                  });



            // Register your app for remote notifications.
            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                // For iOS 10 display notification (sent via APNS)
                UNUserNotificationCenter.Current.Delegate = this;
                var authOptions = UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound;
                UNUserNotificationCenter.Current.RequestAuthorization(authOptions, async (granted, error) =>
                {
                    Console.WriteLine(granted);
                    await System.Threading.Tasks.Task.Delay(500);
                });
            }
            else
            {
                // iOS 9 or before
                var allNotificationTypes = UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound;
                var settings = UIUserNotificationSettings.GetSettingsForTypes(allNotificationTypes, null);
                UIApplication.SharedApplication.RegisterUserNotificationSettings(settings);
            }
            UIApplication.SharedApplication.RegisterForRemoteNotifications();
            Messaging.SharedInstance.Delegate = this;            
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)

        {
            Console.WriteLine($"***** RegisteredForRemoteNotifications");
            Messaging.SharedInstance.ApnsToken = deviceToken;



        }

        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            Console.WriteLine($"***** FailedToRegisterForRemoteNotifications");            

        }

        [Export("messaging:didReceiveRegistrationToken:")]
        public void DidReceiveRegistrationToken(Messaging messaging, string fcmToken)
        {
            /*
             Los tokens de registro se entregan a través del IMessagingDelegatemétodo DidReceiveRegistrationToken. Este método se llama generalmente una vez por inicio de aplicación con un token FCM. Cuando se llama a este método, es el momento ideal para:
             Si el token de registro es nuevo, envíelo a su servidor de aplicaciones (se recomienda implementar la lógica del servidor para determinar si el token es nuevo).
             Suscriba el token de registro a los temas. Esto es necesario solo para nuevas suscripciones o para situaciones en las que el usuario ha reinstalado la aplicación.
             */

            LogInformation(nameof(DidReceiveRegistrationToken), $"Firebase registration token: {fcmToken}");

            //Guardo el token en Settings
            Settings.TokenDispositivo = fcmToken;
        }

        [Export("messaging:didReceiveMessage:")]
        public void DidReceiveMessage(Messaging messaging, RemoteMessage remoteMessage)
        {
            Console.WriteLine($"***** DidReceiveMessage");

            HandleMessage(remoteMessage.AppData, true);
            LogInformation(nameof(DidReceiveMessage), remoteMessage.AppData);
        }

        public override void DidReceiveRemoteNotification(UIApplication application, NSDictionary userInfo, Action<UIBackgroundFetchResult> completionHandler)
        {
            /*
                Si recibe un mensaje de notificación mientras su aplicación está en segundo plano, 
                esta devolución de llamada no se activará hasta que el usuario toque el notificación de inicio de la aplicación. 
                TODO: Manejar los datos de la notificación 
                Con el swizzling deshabilitado, debe informar a Messaging sobre el mensaje
             */

            HandleMessage(userInfo, true);
            // Print full message.
            LogInformation(nameof(DidReceiveRemoteNotification), userInfo);
            completionHandler(UIBackgroundFetchResult.NewData);


        }

        private void HandleMessage(NSDictionary message, bool Open)
        {
            Console.WriteLine($"***** HandleMessage");
            if (message == null)
                return;
            NotificacionModel notificacion = NotificationsServices.NotificacionInicializar();

            try
            {
                foreach (var item in message)
                {
                    switch (item.Key.ToString().ToLower())
                    {
                        case "titulo":
                            notificacion.Titulo = NotificationsServices.NotificacionStringValue(item.Value.ToString());
                            break;
                        case "mensaje":
                            notificacion.Mensaje = NotificationsServices.NotificacionStringValue(item.Value.ToString());
                            break;
                        case "mensajehtml":
                            notificacion.MensajeHtml = NotificationsServices.NotificacionStringValue(item.Value.ToString());
                            break;
                        case "semaforo":
                            notificacion.Semaforo = NotificationsServices.NotificacionStringValue(item.Value.ToString());
                            if (string.IsNullOrEmpty(notificacion.Semaforo))
                            {
                                notificacion.ConSemaforo = false;
                            }

                            try
                            {
                                Color color = new Color();
                                ColorTypeConverter converter = new ColorTypeConverter();

                                color = (Color)(converter.ConvertFromInvariantString(notificacion.Semaforo));
                                notificacion.ConSemaforo = true;

                            }
                            catch
                            {
                                notificacion.Semaforo = "white";
                                notificacion.ConSemaforo = false;
                            }
                            break;
                        case "notificacionid":
                            notificacion.Id = NotificationsServices.NotificacionStringValue(item.Value.ToString());
                            break;
                    }

                }
                if (!string.IsNullOrEmpty(notificacion.MensajeHtml))
                {
                    HtmlToText htt = new HtmlToText();
                    notificacion.Mensaje = htt.ConvertHtml(notificacion.MensajeHtml);
                }
                else
                {
                    notificacion.MensajeHtml = notificacion.Mensaje;
                }

                NotificationsServices.AddNotificacion(notificacion);
                Settings.NumeroNotificaciones = NotificationsServices.NoLeidas();
                CrossBadge.Current.SetBadge(Settings.NumeroNotificaciones);

            }
            catch
            { }

            if (Open)
            {
                Settings.NotificacionSeleccionada = notificacion;

                LoadApplication(new App(notificacion));
                
            }



        }

        void LogInformation(string methodName, object information) => Console.WriteLine($"\nMethod name: {methodName}\nInformation: {information}");

        public static void ShowMessage(string title, string message, UIViewController fromViewController, Action actionForOk = null)
        {
            var alert = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, action => { if (null != actionForOk) { actionForOk.Invoke(); } }));
        }

        public override void ReceivedRemoteNotification(UIApplication application, NSDictionary userInfo)
        {
            /* 
             Si recibe un mensaje de notificación mientras su aplicación está en segundo plano,
             esta devolución de llamada no se activará hasta que el usuario toque la notificación que inicia la aplicación. 
             TODO: Manejar datos de notificación 
             Con el swizzling deshabilitado, debe informar a Messaging sobre el mensaje, para Analytics 
            */
            LogInformation(nameof(ReceivedRemoteNotification), userInfo);
            HandleMessage(userInfo, true);
        }
    }


}
