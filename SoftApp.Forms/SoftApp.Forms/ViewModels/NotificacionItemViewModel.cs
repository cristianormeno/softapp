﻿namespace SoftApp.Forms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Models;
    using Helpers;
    using Notifications;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class NotificacionItemViewModel : NotificacionModel
    {
        #region Propiedades
        public Color ColorFondo01 { get; set; }
        public Color ColorFondo02 { get; set; }

        public Color ColorTexto01 { get; set; }
        public Color ColorTexto02 { get; set; }
        public Color ColorTexto03 { get; set; }

        public Color ColorBorde01 { get; set; }
        public Color ColorBorde02 { get; set; }

        public string TextoContratista { get; set; }

        public string IconoLeido { get; set; }
        #endregion

        #region Commands
        public ICommand SeleccionarNotificacionCommand
        {
            get
            {
                return new RelayCommand(SeleccionarNotificacion);
            }
        }

        #endregion

        #region Metodos

        private async void SeleccionarNotificacion()
        {
            MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(this);
            await App.Navigator.PushAsync(new NotificacionView());
        }


        #endregion
    }
}
