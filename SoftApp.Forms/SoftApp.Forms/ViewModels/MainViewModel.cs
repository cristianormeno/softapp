﻿namespace SoftApp.Forms.ViewModels
{
    using Models;
    using SoftApp.Common.Models;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Reflection;

    public class MainViewModel : BaseViewModel
    {
        #region Attibrutes
        private UsuarioModel usuario;
        #endregion

        #region Properties
        public UsuarioModel Usuario
        {
            get { return this.usuario; }
            set { SetValue(ref this.usuario, value); }
        }
        public ObservableCollection<MenuItemViewModel> Menus
        {
            get;
            set;
        }
        public List<ResponseMenu> Menu { get; set; }
        public string URL { get; set; }

        #endregion

        #region ViewModels


        public LoginViewModel Login { get; set; }
        public HomeViewModel Home { get; set; }
        public NotificacionesViewModel Notificaciones { get; set; }
        public NotificacionViewModel Notificacion { get; set; }
        public ScanViewModel Scan { get; set; }


        //List<LoginMenu> Menu = new List<LoginMenu>();

        #endregion

        #region Constructors
        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();
            
        }
        #endregion

        #region Singleton
        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion

        #region Methods
        public void LoadMenu()
        {
            this.Menus = new ObservableCollection<MenuItemViewModel>();
                        
            //recorro el menu obtenido 
            try
            {
                if (this.Menu.Count > 0)
                {
                    foreach (ResponseMenu item in this.Menu)
                    {
                        string Icono = string.Empty;
                        try
                        {
                            Icono = typeof(Helpers.IconFont).GetField(item.Icono.ToString().Trim()).GetValue(null).ToString();
                        }
                        catch
                        {
                            Icono = Helpers.IconFont.ChevronRight;
                        }

                        
                        
                        this.Menus.Add(new MenuItemViewModel
                        {

                            Tipo = item.Tipo,
                            Icono = Icono,
                            Titulo = item.Titulo,                            
                            Destino = item.Destino,
                            Sistema=item.Sistema,
                            Credenciales=item.Credenciales
                            
                        });

                    }
                }
               
            }
            catch (Exception ex) { }
        }
        #endregion
    }
}
