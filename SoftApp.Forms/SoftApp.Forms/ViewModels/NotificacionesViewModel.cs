﻿namespace SoftApp.Forms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Models;
    using Notifications;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using Xamarin.Forms;

    public class NotificacionesViewModel : BaseViewModel
    {
        #region Attributes
        private ObservableCollection<NotificacionModel> notificacionesCollection;
        private ObservableCollection<NotificacionModel> ListaOrdenada;
        private ObservableCollection<NotificacionItemViewModel> notificaciones;
        private bool estaRecargando;
        #endregion

        #region Properties
        public ObservableCollection<NotificacionItemViewModel> Notificaciones
        {
            get { return this.notificaciones; }
            set { SetValue(ref this.notificaciones, value); }
        }

        public bool EstaRecargando
        {
            get { return this.estaRecargando; }
            set { SetValue(ref this.estaRecargando, value); }
        }
        #endregion

        #region Constructors
        public NotificacionesViewModel()
        {
            this.CargarNotificaciones();
            EliminarCommand = new Command<NotificacionItemViewModel>(async (model) => await EliminarNotificacion(model));
        }

        #endregion

        #region Comandos

        public ICommand RecargarCommand
        {
            get { return new RelayCommand(CargarNotificaciones); }
        }

        public ICommand EliminarCommand { get; private set; }


        #endregion

        #region Metodos


        private void CargarNotificaciones()
        {
            this.EstaRecargando = true;

            this.notificacionesCollection = NotificationsServices.GetNotificaciones();

            ListaOrdenada = new ObservableCollection<NotificacionModel>(this.notificacionesCollection.OrderByDescending(i => i.Fecha));
            this.Notificaciones = new ObservableCollection<NotificacionItemViewModel>(this.ToNotificacionItemViewModel());
            this.EstaRecargando = false;

        }

        private IEnumerable<NotificacionItemViewModel> ToNotificacionItemViewModel()
        {
            Color Color1 = (Color)Application.Current.Resources["ColorPrimary"];
            Color Color2 = (Color)Application.Current.Resources["ColorPrimaryLight"];
            Color Color3 = (Color)Application.Current.Resources["ColorPrimary"];
            Color Color4 = (Color)Application.Current.Resources["ColorPrimary"];
            Color Color5 = (Color)Application.Current.Resources["ColorOnPrimary"];
            Color Color6 = (Color)Application.Current.Resources["ColorOnPrimaryLight"];



            return ListaOrdenada.Select(l => new NotificacionItemViewModel
            {
                Id = l.Id,
                Fecha = l.Fecha,

                Titulo = l.Titulo,
                Mensaje = l.Mensaje,
                MensajeHtml = l.MensajeHtml,
                Semaforo = l.Semaforo,

                ConSemaforo = l.ConSemaforo,

                TextoContratista = "Contratista:",

                Leido = l.Leido,

                ColorFondo01 = (l.Leido) ? Color1 : Color2,
                ColorFondo02 = (l.Leido) ? Color4 : Color4,

                ColorTexto01 = (l.Leido) ? Color5 : Color5,
                ColorTexto02 = (l.Leido) ? Color5 : Color5,
                ColorTexto03 = (l.Leido) ? Color5 : Color5,

                ColorBorde01 = (l.Leido) ? Color6 : Color6,
                ColorBorde02 = (l.Leido) ? Color6 : Color6,

                IconoLeido = (l.Leido) ? Helpers.IconFont.BellOutline : Helpers.IconFont.BellAlert
            }); ;
        }

        private async Task EliminarNotificacion(NotificacionItemViewModel notificacion)
        {
            Acr.UserDialogs.UserDialogs.Instance.Confirm(new Acr.UserDialogs.ConfirmConfig
            {
                CancelText = Languages.No,
                OkText = Languages.Si,
                Message = Languages.EliminarNotificacionMensaje,
                OnAction = async (r) =>
                {
                    if (r)
                    {
                        NotificationsServices.DeleteNotificacion(notificacion.Id);
                        NotificationsServices.NotificacionesPendientes();
                        CargarNotificaciones();
                    }
                }
            });

        }



        #endregion
    }
}
