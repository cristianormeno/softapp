﻿namespace SoftApp.Forms.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Helpers;
    using Models;
    using SoftApp.Common.Models;
    using System.Windows.Input;
    using Views;
    using Xamarin.Forms;

    public class MenuItemViewModel : ResponseMenu
    {
        //[JsonProperty("Grupo")]
        //public string Grupo { get; set; } = string.Empty;

        //[JsonProperty("Icono")]
        //public string Icono { get; set; } = string.Empty;

        //[JsonProperty("Titulo")]
        //public string Titulo { get; set; } = string.Empty;

        //[JsonProperty("Destino")]
        //public string Destino { get; set; } = string.Empty;

        //[JsonProperty("Tipo")]
        //public int Tipo { get; set; } = 0;

        #region Commands
        public ICommand NavigateCommand
        {
            get
            {
                return new RelayCommand(Navigate);
            }
        }

        private void Navigate()
        {
            App.Master.IsPresented = false;
            ScannerPageRequest scannerPageRequest = new ScannerPageRequest();

            switch (this.Tipo)
            {
                case "app":
                    switch (this.Titulo.ToLower())
                    {
                        case "home":
                            MainViewModel.GetInstance().URL = string.Empty;
                            MainViewModel.GetInstance().Home = new HomeViewModel();
                            App.Navigator.PushAsync(new HomeView());
                            break;
                        case "notificaciones":
                            MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();
                            App.Navigator.PushAsync(new NotificacionesView());
                            break;
                        case "salir":
                            Settings.Recordarme = false;
                            UsuarioModel usu = new UsuarioModel()
                            {
                                Usuario = null,
                                Contraseña = null,
                                TokenApiWeb = null,
                                UsuarioID = 0,
                                URL = null
                            };
                            var mainViewModel = MainViewModel.GetInstance();
                            mainViewModel.Usuario = usu;
                            Settings.Usuario = usu;

                            Application.Current.MainPage = new NavigationPage(
                                new LoginView());
                            break;
                    }
                    break;
                case "scan":
                    scannerPageRequest = new ScannerPageRequest()
                    {
                        UsuarioId = Settings.Usuario.UsuarioID,
                        Sistema = this.Sistema,
                        Destino = this.Destino,
                        Valor = string.Empty,
                        EnviarApi = false
                    };
                    MainViewModel.GetInstance().Scan = new ScanViewModel(scannerPageRequest);
                    App.Navigator.PushAsync(new ScanView() { Title = this.Titulo });
                    break;
                case "web":
                    MainViewModel.GetInstance().URL = this.Destino;
                    MainViewModel.GetInstance().Home = new HomeViewModel();
                    App.Navigator.PushAsync(new HomeView() { Title = this.Titulo });
                    break;
                case "scanapi":
                    scannerPageRequest = new ScannerPageRequest()
                    {
                        UsuarioId = Settings.Usuario.UsuarioID,
                        Sistema = this.Sistema,
                        Destino = this.Destino,
                        Valor = string.Empty,
                        EnviarApi=true
                    };
                    MainViewModel.GetInstance().Scan = new ScanViewModel(scannerPageRequest);
                    App.Navigator.PushAsync(new ScanView() { Title = this.Titulo });
                    break;
            }











        }
        #endregion
    }
}
