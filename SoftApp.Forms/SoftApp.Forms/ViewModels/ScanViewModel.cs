﻿using GalaSoft.MvvmLight.Command;
using SoftApp.Common.Models;
using SoftApp.Forms.Helpers;
using SoftApp.Forms.Services;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using ZXing;
using ZXing.Net.Mobile.Forms;

namespace SoftApp.Forms.ViewModels
{
    public class ScanViewModel : BaseViewModel
    {
        #region Servicios
        private ApiService apiService;
        #endregion

        #region Attributes
        private string textoBoton = "Escanear";
        private string resultado = string.Empty;
        private ScannerPageRequest datosScanner = new ScannerPageRequest();
        private bool isVisible = false;
        private string url = string.Empty;
        private bool fromMenu = false;
        
        #endregion

        #region Properties
        public string TextoBoton
        {
            get { return this.textoBoton; }
        }
        public string Url
        {
            get { return this.url; }
            set { SetValue(ref this.url, value); }
        }
        public bool IsVisible
        {
            get { return this.isVisible; }
            set { SetValue(ref this.isVisible, value); }
        }


        public string Resultado
        {
            get { return this.resultado; }
            set { SetValue(ref this.resultado, value); }
        }

        public ScannerPageRequest DatosScanner {
            get { return this.datosScanner; }
            set { SetValue(ref this.datosScanner, value); }
        }

        public bool FromMenu
        {
            get { return this.fromMenu; }
            set { SetValue(ref this.fromMenu, value); }
        }
        #endregion

        #region Constructors
        public ScanViewModel()
        {
            this.apiService = new ApiService();
        }

        public ScanViewModel(ScannerPageRequest scannerPageRequest)
        {
            this.apiService = new ApiService();
            this.datosScanner = scannerPageRequest;
            this.fromMenu = true;
            
            
        }
        #endregion

        #region Commands

        public ICommand ScanCommand
        {
            get
            {
                return new RelayCommand(Scann);
            }
        }

        
        #endregion

        #region Methods
        public async void Scann()
        {
            this.IsVisible = false;
            this.fromMenu = false;
            var scannerPage = new ZXingScannerPage();
            //MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(this);
            await App.Navigator.PushAsync(scannerPage);
            scannerPage.OnScanResult += (Result) =>
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    await App.Navigator.PopAsync();

                    this.Resultado = Result.Text;
                    if (this.DatosScanner.EnviarApi)
                    {// Llamar al Api, para obtener la url con el código escaneado
                        this.DatosScanner.Valor = Result.Text;
                        // llamo al ws
                        var api = Application.Current.Resources["APIWeb"].ToString();
                        var apiBase = Application.Current.Resources["APIWebBase"].ToString() + "/";

                        ScannerPageResponse resultado = new ScannerPageResponse();
                        resultado = await this.apiService.ScannerApi(api, apiBase, "ScannerPage", this.DatosScanner);

                        
                        if (resultado == null)
                        {
                            MostrarMensaje("Languages.Error", Languages.AlgoSalioMal);
                            return;
                        }

                        this.Url = resultado.URL;
                        this.IsVisible = true;
                    }
                    else
                    { // reemplazar el parámetro con el código escaneado
                        this.Url = this.DatosScanner.Destino.Replace("xxxcodxxx", Result.Text.Trim());
                        this.IsVisible = true;                       
                    }



                });
            };


        }

        private void MostrarMensaje(string Titulo, string Mensaje)
        {
            //UserDialogs.Instance.Alert(Mensaje,"Error","Aceptar");
            Application.Current.MainPage.DisplayAlert(Mensaje, "Error", Languages.Aceptar);
            try { Acr.UserDialogs.UserDialogs.Instance.HideLoading(); } catch { }
            
        }
        #endregion












    }
}
