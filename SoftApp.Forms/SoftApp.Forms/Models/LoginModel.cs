﻿namespace SoftApp.Forms.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    public class LoginRequest
    {
        public LoginUsuario Usuario { get; set; }
        public LoginDispositivo Dispositivo { get; set; }
    }

    public class LoginResponse
    {
        public ResponseResultado Respuesta { get; set; }
        public ResponseUsuario Usuario { get; set; }
        public ResponseSistema Sistema { get; set; }
        public List<MenuModel> Menu { get; set; }
    }

    public partial class LoginUsuario
    {
        public string Usuario { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string Mandante { get; set; } = string.Empty;
    }
    public partial class LoginDispositivo
    {
        public string Token { get; set; } = string.Empty;

        public int Sistema { get; set; } = 0;
    }

    public partial class ResponseResultado
    {
        [JsonProperty("Resultado")]
        public string Resultado { get; set; } = string.Empty;

        [JsonProperty("Mensaje")]
        public string Mensaje { get; set; } = string.Empty;

        
    }

    public partial class ResponseUsuario
    {
        [JsonProperty("UsuarioId")]
        public int UsuarioId { get; set; } = 0;

        [JsonProperty("Nombre")]
        public string Nombre { get; set; } = string.Empty;

        [JsonProperty("TokenApiWeb")]
        public string TokenApiWeb { get; set; } = string.Empty;
    }

    public partial class ResponseSistema
    {
        [JsonProperty("VersionMinima")]
        public int VersionMinima { get; set; } = 0;

        [JsonProperty("VersionActual")]
        public int VersionActual { get; set; } = 0;

        [JsonProperty("VersionActualNombre")]
        public string VersionActualNombre { get; set; } = string.Empty;
        
        [JsonProperty("URL")]
        public string URL { get; set; } = string.Empty;
    }

   
}
