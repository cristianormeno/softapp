﻿using System;

namespace SoftApp.Forms.Models
{
    public class NotificacionModel
    {
        //--- Datos
        public string Id { get; set; }
        public DateTime Fecha { get; set; }
        public string Titulo { get; set; }
        public string Mensaje { get; set; }

        public string MensajeHtml { get; set; }

        public string Semaforo { get; set; }
        

        //--- Formato
        public bool ConSemaforo { get; set; }
        

        //--- sólo control
        public bool Leido { get; set; }

    }
}
