﻿namespace SoftApp.Forms.Notifications
{
    using Helpers;
    using Models;
    using SoftApp.Forms.ViewModels;
    using SoftApp.Forms.Views;
    using Newtonsoft.Json;
    using Plugin.Badge;
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Xamarin.Forms;

    public class NotificationsServices
    {
        #region Metodos Publicos

        // Método que devuelve el listado de Notificaciones
        public static ObservableCollection<NotificacionModel> GetNotificaciones()
        {
            if (!string.IsNullOrEmpty(Settings.Notificaciones))
            {
                ObservableCollection<NotificacionModel> Lista;
                Lista = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
                return Lista;
            }
            return new ObservableCollection<NotificacionModel>();
        }

        // Método que agrega una notificación, recibiendo un objeto como parámetro
        public static ObservableCollection<NotificacionModel> AddNotificacion(NotificacionModel notificacion)
        {
            ObservableCollection<NotificacionModel> notificaciones;

            //Obtengo el listado actual de notificaciones
            if (!string.IsNullOrEmpty(Settings.Notificaciones))
                notificaciones = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
            else
                notificaciones = new ObservableCollection<NotificacionModel>();

            if (string.IsNullOrEmpty(notificacion.Id))
            {
                // La notificación no tiene un identificador, por lo tanto no se agregará al listado
                return notificaciones;
            }

            if (!YaExiste(notificacion.Id))
            {
                //Si no existe la notificación, la agrego
                notificaciones.Add(new NotificacionModel
                {
                    Id = notificacion.Id,

                    Fecha = DateTime.Now,
                    Titulo = notificacion.Titulo,
                    Mensaje = notificacion.Mensaje.Replace("*** ", System.Environment.NewLine),
                    MensajeHtml=notificacion.MensajeHtml,
                    Semaforo = notificacion.Semaforo,
                    
                    ConSemaforo = notificacion.ConSemaforo,
                    Leido = false,
                });
                Settings.Notificaciones = JsonConvert.SerializeObject(notificaciones);
            }
            return notificaciones;
        }

        // Método que elimina una notificación, recibiendo como parámetro el Id
        public static ObservableCollection<NotificacionModel> DeleteNotificacion(string id)
        {
            ObservableCollection<NotificacionModel> notificaciones;

            if (!string.IsNullOrEmpty(Settings.Notificaciones))
                notificaciones = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
            else
                notificaciones = new ObservableCollection<NotificacionModel>();

            var notificacion = notificaciones.Where(n => n.Id == id).FirstOrDefault();

            if (notificacion != null)
            {
                notificaciones.Remove(notificacion);
                Settings.Notificaciones = JsonConvert.SerializeObject(notificaciones);
            }

            return notificaciones;

        }

        // Método que marca como leída una notificación, recibiendo como parámetro el Id
        public static ObservableCollection<NotificacionModel> MarcarLeidaNotificacion(string id)
        {
            ObservableCollection<NotificacionModel> notificaciones;

            if (!string.IsNullOrEmpty(Settings.Notificaciones))
                notificaciones = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
            else
                notificaciones = new ObservableCollection<NotificacionModel>();

            var notificacion = notificaciones.Where(n => n.Id == id).FirstOrDefault();

            if (notificacion != null)
            {
                notificacion.Leido = true;
                Settings.Notificaciones = JsonConvert.SerializeObject(notificaciones);
            }

            return notificaciones;

        }


        // Método que devuelve la cantidad de notificaciones No leídas
        public static int NoLeidas()
        {
            int Cantidad = 0;

            ObservableCollection<NotificacionModel> notificaciones;
            if (!string.IsNullOrEmpty(Settings.Notificaciones))
            {
                notificaciones = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
            }
            else
            {
                notificaciones = new ObservableCollection<NotificacionModel>();
            }

            var noleidas = notificaciones.Where(n => !n.Leido);

            try
            {
                Cantidad = noleidas.Count();
            }
            catch
            {
                Cantidad = 0;
            }

            return Cantidad;
        }

        // Método que comprueba si existe o no una notificación según su Id
        public static bool YaExiste(string Id)
        {
            int Cantidad = 0;

            ObservableCollection<NotificacionModel> notificaciones;
            if (!string.IsNullOrEmpty(Settings.Notificaciones))
            {
                notificaciones = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
            }
            else
            {
                notificaciones = new ObservableCollection<NotificacionModel>();
            }

            var MismoId = notificaciones.Where(n => n.Id.Trim() == Id);

            try
            {
                Cantidad = MismoId.Count();
            }
            catch
            {
                Cantidad = 0;
            }

            if (Cantidad > 0)
                return true;
            else
                return false;


        }


        // Método que devuelve una notificación, recibiendo como parámetro el Id
        public static NotificacionModel GetNotificacion(string Id)
        {
            NotificacionModel notif = new NotificacionModel();

            ObservableCollection<NotificacionModel> notificaciones;
            if (!string.IsNullOrEmpty(Settings.Notificaciones))
            {
                notificaciones = JsonConvert.DeserializeObject<ObservableCollection<NotificacionModel>>(Settings.Notificaciones);
            }
            else
            {
                notificaciones = new ObservableCollection<NotificacionModel>();
            }

            try
            {
                notif = notificaciones.Where(n => n.Id == Id).FirstOrDefault();
            }
            catch { }

            return notif;

        }


        // Método para crear una instancia de notificación a partir de los datos recibidos en la notificación
        // recibe como parámetro el conjunto de claves y valors
        [Obsolete]
        public static void CreateNotificacion(System.Collections.Generic.IDictionary<string, object> data)
        {
            NotificacionModel notificacion = NotificacionInicializar();

            try
            {
                foreach (var key in data)
                {
                    switch (key.Key.ToLower()) {
                        case "titulo":
                            notificacion.Titulo = NotificacionStringValue(key.Value.ToString());
                            break;
                        case "mensaje":
                            notificacion.Mensaje = NotificacionStringValue(key.Value.ToString());
                            break;
                        case "mensajehtml":
                            notificacion.MensajeHtml = NotificacionStringValue(key.Value.ToString());
                            break;
                        case "semaforo":
                            notificacion.Semaforo = NotificacionStringValue(key.Value.ToString());
                            if (string.IsNullOrEmpty(notificacion.Semaforo))
                            {
                                notificacion.ConSemaforo = false;
                            }

                            try
                            {
                                Color color = new Color();
                                ColorTypeConverter converter = new ColorTypeConverter();

                                color = (Color)(converter.ConvertFromInvariantString(notificacion.Semaforo));
                                notificacion.ConSemaforo = true;

                            }
                            catch
                            {
                                notificacion.Semaforo = "white";
                                notificacion.ConSemaforo = false;
                            }
                            break;
                        case "notificacionid":
                            notificacion.Id = NotificacionStringValue(key.Value.ToString());
                            break;
                    }                    
                }

                if (!string.IsNullOrEmpty(notificacion.MensajeHtml)) 
                {
                    HtmlToText htt = new HtmlToText();
                    notificacion.Mensaje = htt.ConvertHtml(notificacion.MensajeHtml);
                }
                else
                {
                    notificacion.MensajeHtml = notificacion.Mensaje;
                }

                AddNotificacion(notificacion);
                Settings.NumeroNotificaciones = NoLeidas();
                CrossBadge.Current.SetBadge(Settings.NumeroNotificaciones);
            }
            catch { }

        }

        // Método devuelve la notificación creada para su su apertura desde inicio
        public static NotificacionModel NotificacionCreadaAcceso(System.Collections.Generic.IDictionary<string, object> data)
        {
            //NotificacionItemViewModel notificacion = new NotificacionItemViewModel();
            NotificacionModel notificacion = new NotificacionModel();
            try
            {

                string Id = string.Empty;

                //Intento crear la notificación, por si no ingresó por el proceso de recibir la notificación
                NotificationsServices.CreateNotificacion(data);

                //obtengo el Id de la notificación ingresada
                foreach (var key in data)
                {
                    if (key.Key == "NotificacionId")
                        Id = NotificacionStringValue(key.Value.ToString());
                }

                if (string.IsNullOrEmpty(Id))
                    return null;

                notificacion = GetNotificacion(Id);

                // Cuando la notifiación seleccionada no existe, traer la última ingresada
                if (notificacion == null)
                {
                    notificacion = GetNotificaciones().Last();

                }

            }
            catch { }
            return notificacion;

        }


        // Método que actualiza el número de notificaciones pendientes
        public static void NotificacionesPendientes()
        {
            Settings.NumeroNotificaciones = NotificationsServices.NoLeidas();
            CrossBadge.Current.SetBadge(Settings.NumeroNotificaciones);

            if (Settings.NumeroNotificaciones == 0)
            {
                CrossBadge.Current.ClearBadge();
            }
        }

        // Método que devuelve el valor en formato correcto para valores string
        public static string NotificacionStringValue(string valor)
        {
            if (string.IsNullOrEmpty(valor))
                return string.Empty;
            else
                return valor;
        }

        // Método que devuelve el valor en formato correcto para valores booleanos
        public static bool NotificacionBoolValue(string valor)
        {
            if (string.IsNullOrEmpty(valor))
                return true;
            else
                if (valor.Trim().ToLower() == "false")
                return false;
            else
                return true;
        }

        // Método completa todos los campos de la notificación para evitar valores null
        public static NotificacionModel NotificacionInicializar()
        {
            NotificacionModel notificacion = new NotificacionModel
            {
                Id = string.Empty,
                Fecha = DateTime.Now,

                Titulo = string.Empty,
                Mensaje = string.Empty,
                MensajeHtml = string.Empty,
                Semaforo = string.Empty,

                ConSemaforo = false,
                Leido = false
            };
            return notificacion;
        }


        public static void NotificacionOpen()
        {
            if (Settings.NotificacionSeleccionada != null && !string.IsNullOrEmpty(Settings.TokenDispositivo) && !string.IsNullOrEmpty(Settings.Usuario.TokenApiWeb))
            {
                MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(Settings.NotificacionSeleccionada);

                //if (Device.RuntimePlatform.ToString().ToUpper() != "ANDROID")
                //{
                //    Settings.NotificacionSeleccionada = null;
                //}
                App.Navigator.PushAsync(new NotificacionView());
            }
        }
        #endregion

        #region Metodos Privados


        #endregion
    }
}
