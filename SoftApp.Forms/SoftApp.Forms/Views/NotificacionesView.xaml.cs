﻿namespace SoftApp.Forms.Views
{
    using SoftApp.Forms.ViewModels;
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NotificacionesView : ContentPage
    {
        public NotificacionesView()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasBackButton(this, false);
            MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();


        }
    }
}