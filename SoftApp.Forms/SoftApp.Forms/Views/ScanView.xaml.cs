﻿using SoftApp.Forms.Interfaces;
using SoftApp.Forms.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SoftApp.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ScanView : ContentPage
    {
        public ScanView()
        {
            InitializeComponent();            
        }

        void webviewNavigating(object sender, WebNavigatingEventArgs e)
        {
            labelLoading.Text = Helpers.Languages.Cargando + " ... ";
            labelLoading.IsVisible = true;
        }
        void webviewNavigated(object sender, WebNavigatedEventArgs e)
        {
            var htmlSource = new HtmlWebViewSource();
            htmlSource.BaseUrl = DependencyService.Get<IBaseUrl>().Get();
            htmlSource.Html = @"<html>
                  <head>
                    <title>Error</title>
                  </head>
                  <body>
                    <h1>Error:</h1>
                    <p>No hemos podido acceder a la información</p>
                    <img src='Logo.png'/>
                  </body >
                </html > ";

            switch (e.Result)
            {
                case WebNavigationResult.Cancel:
                    labelLoading.Text = "CANCEL" + " --> " + e.Result.ToString();
                    labelLoading.IsVisible = false;
                    break;
                case WebNavigationResult.Failure:
                    labelLoading.Text = "FAILURE" + " --> " + e.Result.ToString();
                    labelLoading.IsVisible = false;
                    WebView1.Source = htmlSource;
                    break;
                case WebNavigationResult.Success:
                    labelLoading.Text = "SUCCESS" + " --> " + e.Result.ToString();
                    labelLoading.IsVisible = false;
                    break;
                case WebNavigationResult.Timeout:
                    labelLoading.Text = "TIMEOUT" + " --> " + e.Result.ToString();
                    labelLoading.IsVisible = false;
                    WebView1.Source = htmlSource;
                    break;
                default:
                    labelLoading.Text = "DEFAULT" + " --> " + e.Result.ToString();
                    labelLoading.IsVisible = false;
                    break;

            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            NavigationPage.SetHasBackButton(this, false);
            if (MainViewModel.GetInstance().Scan.FromMenu)
            {
                MainViewModel.GetInstance().Scan.Scann();
            }
            
            
        }

        //    private void scanView_OnScanResult(ZXing.Result result)
        //    {
        //        Device.BeginInvokeOnMainThread(async () =>
        //        {
        //            await DisplayAlert("Scanned result", "The barcode's text is " + result.Text + ". The barcode's format is " + result.BarcodeFormat, "OK");
        //        });
        //    }
    }
    }