﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SoftApp.Forms.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuView : ContentPage
    {
        public MenuView()
        {
            InitializeComponent();
        }
    }
}