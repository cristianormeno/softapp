﻿namespace SoftApp.Forms.Helpers
{
    using Models;
    using Services;
    using SoftApp.Common.Models;
    using SoftApp.Forms.Views;
    using System.Collections.Generic;
    using ViewModels;
    using Xamarin.Forms;

    public class Autenticar
    {
        #region Attributes
        private ApiService apiService;
        #endregion

        #region Methods
        public async void Login()
        {
            UsuarioModel usuario = new UsuarioModel();
            usuario = Settings.Usuario;

            // Validar que los campos requeridos estén completos
            if (string.IsNullOrEmpty(usuario.Usuario))
            {
                Settings.Usuario.TokenApiWeb = null;
                Application.Current.MainPage = new NavigationPage(new LoginView());
                return;
            }

            if (string.IsNullOrEmpty(usuario.Contraseña))
            {
                Settings.Usuario.TokenApiWeb = null;
                Application.Current.MainPage = new NavigationPage(new LoginView());
                return;
            }

            
            // Obtengo la prueba de conexión
            this.apiService = new ApiService();
            var conexion = await this.apiService.CheckConnection();

            if (!conexion.IsSuccess)
            {
                Settings.Usuario.TokenApiWeb = null;
                Application.Current.MainPage = new NavigationPage(new LoginView());
                return;
            }

            // Obtengo el sistema operativo del dispositivo
            Common comunes = new Common();
            int Sistema = comunes.GetSistemaDispositivo;
            string Token = comunes.GetTokenSistema;

            LoginRequest Datos = new LoginRequest()
            {
                Usuario = new LoginUsuario()
                {
                    Usuario = usuario.Usuario,
                    Password = usuario.Contraseña
                },
                Dispositivo = new LoginDispositivo()
                {
                    Token = Token,
                    Sistema = Sistema
                }
            };

            // llamo al ws
            var api = Application.Current.Resources["APIWeb"].ToString();
            var apiBase = Application.Current.Resources["APIWebBase"].ToString() + "/";

            LoginResponse resultado = new LoginResponse();

            resultado = await this.apiService.Login(api, apiBase, "Login", Datos);

            if (resultado == null)
            {
                Settings.Usuario.TokenApiWeb = null;
                Application.Current.MainPage = new NavigationPage(new LoginView());
                return;
            }

            if (string.IsNullOrEmpty(resultado.Usuario.TokenApiWeb))
            {
                Settings.Usuario.TokenApiWeb = null;
                Application.Current.MainPage = new NavigationPage(new LoginView());
                return;
            }

            if (!VersionUnica.ValidarVersion(resultado.Sistema.VersionMinima))
            {
                Settings.Usuario.TokenApiWeb = null;
                Application.Current.MainPage = new NavigationPage(new LoginView());
                return;
            }

            UsuarioModel usu = new UsuarioModel()
            {
                UsuarioID = resultado.Usuario.UsuarioId,
                Usuario = usuario.Usuario,
                Contraseña = usuario.Contraseña,
                TokenApiWeb = resultado.Usuario.TokenApiWeb,
                Nombre = resultado.Usuario.Nombre,
                URL = resultado.Sistema.URL
            };

            List<ResponseMenu> Menu = new List<ResponseMenu>();

            int i = 0;
            int salir = -1;

            salir = resultado.Menu.FindIndex(item => item.Titulo.ToLower().Trim() == "salir");

            foreach (ResponseMenu Item in resultado.Menu)
            {
                Menu.Add(Item);
                i++;
            }

            if (salir < 0)
            {
                //Agrego el menú Salir
                Menu.Add(new ResponseMenu()
                {
                    Tipo = "app",
                    Icono = "ExitToApp",
                    Titulo = "Salir",
                    Destino = ""
                });
            }

            // Guardo los datos del usuario
            var mainViewModel = MainViewModel.GetInstance();
            mainViewModel.Usuario = usu;
            Settings.Usuario = usu;
            mainViewModel.Menu = Menu;
            mainViewModel.LoadMenu();
            //Llamo a la siguiente vista
            mainViewModel.Home = new HomeViewModel();
            Application.Current.MainPage = new MasterView();





            if (Device.RuntimePlatform.ToString().ToUpper() == "ANDROID")
            {
                if (Settings.NotificacionSeleccionada != null && !string.IsNullOrEmpty(Settings.TokenDispositivo) && !string.IsNullOrEmpty(Settings.Usuario.TokenApiWeb))
                {
                    MainViewModel.GetInstance().Notificacion = new NotificacionViewModel(Settings.NotificacionSeleccionada);
                    //MainViewModel.GetInstance().Notificaciones = new NotificacionesViewModel();
                    Settings.NotificacionSeleccionada = null;
                    await App.Navigator.PushAsync(new NotificacionView());
                    //  App.Navigator.PushAsync(new NotificacionesView());
                }
            }

            return;
        }
        #endregion
    }
}
