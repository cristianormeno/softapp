﻿namespace SoftApp.Forms.Helpers
{
    using Xamarin.Forms;
    using Interfaces;
    using Resources;

    public static class Languages
    {
        static Languages()
        {
            var ci = DependencyService.Get<ILocalize>().GetCurrentCultureInfo();
            Resource.Culture = ci;
            DependencyService.Get<ILocalize>().SetLocale(ci);
        }



        public static string Aceptar
        {
            get { return Resource.Aceptar; }
        }

        public static string AlgoSalioMal
        {
            get { return Resource.AlgoSalioMal; }
        }

        public static string Cargando
        {
            get { return Resource.Cargando; }
        }

        public static string CerrarSesion
        {
            get { return Resource.CerrarSesion; }
        }

        public static string Contraseña
        {
            get { return Resource.Contraseña; }
        }

        public static string DebesIngresarUnaContraseña
        {
            get { return Resource.DebesIngresarUnaContraseña; }
        }

        public static string DebesIngresarUnMandante
        {
            get { return Resource.DebesIngresarUnMandante; }
        }

        public static string DebesIngresarUnUsuario
        {
            get { return Resource.DebesIngresarUnUsuario; }
        }

        public static string Eliminar
        {
            get { return Resource.Eliminar; }
        }

        public static string EliminarNotificacionMensaje
        {
            get { return Resource.EliminarNotificacionMensaje; }
        }

        public static string Entrar
        {
            get { return Resource.Entrar; }
        }

        public static string Error
        {
            get { return Resource.Error; }
        }

        public static string EsperaUnMomento
        {
            get { return Resource.EsperaUnMomento; }
        }

        public static string Home
        {
            get { return Resource.Home; }
        }

        public static string Login
        {
            get { return Resource.Login; }
        }

        public static string Mandante
        {
            get { return Resource.Mandante; }
        }

        public static string No
        {
            get { return Resource.No; }
        }

        public static string Notificacion
        {
            get { return Resource.Notificacion; }
        }

        public static string Notificaciones
        {
            get { return Resource.Notificaciones; }
        }

        public static string PreguntaNuevaNotificacion
        {
            get { return Resource.PreguntaNuevaNotificacion; }
        }

        public static string RecordarUsuario
        {
            get { return Resource.RecordarUsuario; }
        }

        public static string ReviseCasilla
        {
            get { return Resource.ReviseCasilla; }
        }

        public static string Si
        {
            get { return Resource.Si; }
        }

        public static string Usuario
        {
            get { return Resource.Usuario; }
        }

        public static string VersionMinima
        {
            get { return Resource.VersionMinima; }
        }
    }
}
