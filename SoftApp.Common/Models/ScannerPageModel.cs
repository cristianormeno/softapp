﻿using System;

namespace SoftApp.Common.Models
{
    public partial class ScannerPageRequest
    {
        public long UsuarioId { get; set; } = 0;
        public string Sistema { get; set; } = string.Empty;
        public string Destino { get; set; } = string.Empty;
        public string Valor { get; set; } = string.Empty;
        public bool EnviarApi { get; set; } = false;
    }

    public partial class ScannerPageResponse 
    {
        public string URL { get; set; } = string.Empty;
    }
}
